import axios from "axios"
import { createContext, useState, useEffect } from "react"
import { useHistory } from "react-router"
export const LoginContext = createContext()
import { SERVER_URL } from "../globals"

export default function Login({ children }) {
    console.log(SERVER_URL)
    const loginState = useState(),
        [user, setUser] = loginState,
        [mode, setMode] = useState('login'),
        [error, setError] = useState('')

    // [error, serError] = useState('')




    async function login(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        loginUser(values)

    }

    async function loginUser(values) {

        try {
            const res = await axios.post(`${SERVER_URL}/${mode}`, values)

            if (values.stay)
                localStorage.token = res.data.token
            else
                sessionStorage.token = res.data.token
            setUser(res.data)
            setError('')
        }
        catch (e) {
            // console.log(e.response?.data?.error || e.message)
            // debugger
            setError(e.response.data.error.name ? 'email or pass exist' : e.response.data.error)
        }

    }


    function register() {
        setMode('register')
        setError('')
    }
    function log() {
        setMode('login')
        setError('')
    }

    useEffect(async () => {
        if (localStorage.token) {
            const use = await axios.post(`${SERVER_URL}/user`,
                {},
                { headers: { authorization: localStorage.token } })
            setUser(use.data)
        }
    }
        , [])


    return <LoginContext.Provider value={loginState}>
        {user ? <div>{children}</div> : (mode == 'register' ?

            <div className="login"
            ><form onSubmit={login}>
                    <h1 className="LOGIN">SIGN UP</h1>
                    <div>{error}</div>
                    <div className="loginbox">
                        <input className="loginInput" type="text" placeholder="email" name="email" required />
                        <input className="loginInput" type="password" placeholder="password" name="password" required />
                        <input className="loginInput" type="text" placeholder="full name" name="full_name" required />
                    </div>
                    <div className="stayConnect">
                        <input className="Login" type="submit" value="sign up" />
                        <label>stay connect?<input type="checkbox" name="stay" /></label>
                    </div>
                </form>
                <div className="signUp">
                    <label className="register?">OR </label>
                    <button className="theSubmit" onClick={log} > log in</button>
                </div>
            </div>
            :

            <div className="login">
                <form onSubmit={login}>
                    <h1 className="LOGIN">LOGIN</h1>
                    <div className='err'>{error}</div>
                    <div className="loginbox">
                        <input className="loginInput" type="text" placeholder="email" name="email" required />
                        <input className="loginInput" type="password" placeholder="password" name="password" required />
                    </div>
                    <div className="stayConnect">
                        <input className="Login" type="submit" value="login" />
                        <label className="connect">stay connect
                            <input type="checkbox" name="stay" /></label>
                    </div>
                </form>
                <div className="signUp">
                    <label className="register?">OR </label>
                    <button className="theSubmit" onClick={register} >sign up</button>
                </div>
            </div>
        )}


    </LoginContext.Provider>
}
