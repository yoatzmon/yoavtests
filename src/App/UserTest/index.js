import axios from "axios"
import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { LoginContext } from "../../Login"
import { SERVER_URL } from "../../globals"
export default function UserTest() {

    const [user,setUser] = useContext(LoginContext)
    console.log(user)
    const [userTest, setUserTest] = useState('')
    const [testDetails, setTestDetails] = useState('')
    const { test } = useParams()
    const [sec, setSec] = useState('')
    const [error,setError] = useState('')
    const [err,setErr] = useState('')
    // console.log(userTest[0])
    console.log(sec)

    async function getTest() {
        const readTest = await axios.get(`${SERVER_URL}/usertest?id=${test}`,{ headers: { authorization: localStorage.token||sessionStorage.token } })
        console.log(readTest.data)
        setUserTest(readTest.data)

    }

    async function getTestDetails() {
        const readTestDetails = await axios.get(`${SERVER_URL}/testsdetails?id=${test}`,{ headers: { authorization: localStorage.token||sessionStorage.token } })
        console.log(readTestDetails.data)
        setTestDetails(readTestDetails.data)
    }


    async function sendTest(event) {
        try{
        setError('')
        event.preventDefault()
        const values = Object.values(event.target)

        const userPick = values.filter(item => item.checked === true)
      
        let sketch = []
        for (let i of userPick){
            sketch.push({
                qua_score: i.attributes.qua_score.value,
                answer_score: i.attributes.answer_score.value,
                question_id: i.attributes.id.value,
                answer: i.attributes.answer.value,
                ques_type : i.attributes.ques_type.value
            })
        }
        
    
        console.log(sketch);
        
        let counter;
        let sum = 0
        for(let count of sketch){
            if(count.ques_type == 'checkbox'){
                if(counter != count.question_id || sum == 1||0){
                    sum = sum + 1;
                    counter = count.question_id
                }
                else{
                    throw `you entered more or less option the you have to`
                }
            }

        }



        await makeSketch(sketch)
    }
        catch(e){
            e.response.data?
            setErr(e.response.data)
            :
            setError(e)
        }

    }


    async function makeSketch(values) {
            const detail = await axios.post(`${SERVER_URL}/makesketch?id=${test}`, values , { headers: { authorization: localStorage.token||sessionStorage.token } })
            console.log(detail)
            setSec('THE TEST SEND!')
   

    }

    useEffect(getTest, [])
    useEffect(getTestDetails, [])

    return userTest ?
        (testDetails.find(u => u.user_id ==  user._id).status == 'done' ?
            <div className="doneTest">
                <div>
                    <h1 className="titles"> {userTest[0].name} </h1>
                    <h3 className="titles"> {userTest[0].discription}</h3>
                </div>
                <form>
                    {userTest[1].map(item =>
                        <div className="question">
                            <h3 className="titles">{item.name}</h3>
                            <h4 className="titles">{item.Instructions}</h4>
                            <div className="answers">
                                {item.answers.map(ans =>
                                    <label  className="answer">
                                        {ans.discription} {`    `}
                                        <input
                                            type={item.ques_type}
                                            name={ans}
                                            defaultChecked={testDetails.find(item=> item.sketch.find(item => item.answer == ans._id))}
                                           
                                        />
                                    </label>

                                )}
                            </div>
                        </div>
                    )}
                    <hr></hr>
                </form>
            </div>

            :
            <div className="doneTest">
                <div>
                    <h1 className="titles">name:   {userTest[0].name} </h1>
                    <h3 className="titles">discription : {userTest[0].discription}</h3>
                </div>
                <form onSubmit={sendTest}>
                    {userTest[1].map((item, i) =>
                        <div className="question">
                            <h3 className="titles">{item.name}</h3>
                            <h4 className="titles">{item.Instructions}</h4>
                            <div className="answers">
                                {item.answers.map((ans, index) =>
                                    <label className="answer" >{ans.discription}
                                        <input
                                            type={item.ques_type}
                                            name='1'
                                            answer_score={ans.score}
                                            qua_score={item.score}
                                            id={i}
                                            answer={ans._id}
                                            ques_type={item.ques_type}
                                        />
                                    </label>

                                )}
                            </div>
                        </div>
                    )}
                    <hr></hr>
                   {!sec ? <input className="newTest" type='submit' value="send test" /> : ''}
                </form>
                <div>{sec}</div>
                <div>{error}</div>
                <div>{err}</div>
            </div>

        ) : 'loading'

}