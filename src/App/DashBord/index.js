import { useEffect, useState } from "react";
import axios from "axios";
import { useContext } from "react";
import { LoginContext } from '../../Login'
import { Link } from "react-router-dom"
import { SERVER_URL } from "../../globals";
export default function DashBord() {
  
  
  const [dash, setDash] = useState('')
  const [del, setDel] = useState('')
  const[error,setError] = useState('')
  // console.log(error)
  const user = useContext(LoginContext)
  
  async function fetchData() {

    const res = await axios.get(`${SERVER_URL}/dash?id=${user[0]._id}`)
    console.log(res.data)
    setDash(res.data)
    // console.log(res.data);
    
  }
  
  
  //   async function getTestDetails() {
    //     const readTestDetails = await axios.get(`http://localhost:4000/testsdetails?id=${test}`)
    //     console.log(readTestDetails.data)
    //     setTestDetails(readTestDetails.data)
    // }
    
    async function unActive(e,item) {
      try{
      setError('')
      // console.log(item._id)
      await axios.put(`${SERVER_URL}/unactive?id=${item._id}`)
       setDel([...del, {}])
    }
    catch(e){
      // debugger
      setError(e.response.data)
    }
      
  }


  useEffect(fetchData, [del])
  // useEffect(unActive, [])


  return dash ?
    <div className="testImade">
      <div className="title_middle">
      <h1 className="title">test i create</h1>
      </div>
      <hr></hr>
      <table>
        <tr>
          <th>Name</th>
          <th>discription</th>
          <th>status</th>
          <th>grades</th>
        </tr>
        {dash.map(item =>
          item.active ?
            <tr>
              {item.status == 'Edit' ?

                <Link to={`/test-form/${item._id}`}>
                  <td>{item.name}</td></Link>
                :
                <Link to={`/readtest/${item._id}`}>
                  <td >{item.name}</td></Link>
              }
              <td>{item.discription}</td>
              <td>{item.status}</td>
              <td>
              {item.students_grades.map(i =>
                i.name ? 
               <div> {i.name} - {i.grade}</div> : ''
                )}
              </td>
              <td className="deleteTest" onClick={(e)=>unActive(e,item)}>delete test</td>


            </tr>
            : '')}

        <div>
          <Link to='/test-form'>
            <button class="newTest">+</button>
          </Link>
        </div>
        {error} 
      </table>
    </div >
    : 'loading'
}