import { Route, Switch, Link, useHistory } from "react-router-dom"
import DashBord from './DashBord'
import DashTest from './DashTest'
import TestForm from './TestForm'
import UserTest from './UserTest'
import ReadTest from './ReadTest'
import Login from '../Login'
import { useContext, useState } from "react";
import { LoginContext } from '../Login'
import './App.css'

function App() {
    const History = useHistory()
    const [user, setUser] = useContext(LoginContext)
    // console.log(user)
    function logout() {
        delete localStorage.token
        setUser()
        History.push('/')
    }

    return <div className="App">
        <div className="header">
            <img className="logo" src="https://img.icons8.com/dotty/80/000000/face-id.png" />
            
                {user ? <h3 className="hey"> {user.full_name}
                <button className="butLogout" onClick={logout}>log out</button>
                </h3> : ''}
                
            
            <div className="navigation">
                <Link to='/' className="manu">home</Link>
                <div>|</div>
                <Link to='/my-tests' className="manu"> mytest</Link>
            </div>
        </div>
        
        {/* <Login> */}
        <Switch>
        <div  className="backroundPicture">
            <Route path='/' component={DashBord} exact></Route>{/* create tests*/}
            <Route path='/my-tests' component={DashTest} ></Route>{/* assigned tests */}
            <Route path='/test/:test' component={UserTest} ></Route>
            <Route path='/test-form/:testId?' component={TestForm} ></Route>
            <Route path='/readtest/:test' component={ReadTest} ></Route>
        </div>
            {/* create/edit */}
        </Switch>
        {/* </Login> */}
    </div>
}

export default App
