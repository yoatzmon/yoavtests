import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { SERVER_URL } from "../../globals"
export default function ReadTest() {

    const [userTest, setUserTest] = useState('')
    const [testDetails, setTestDetails] = useState('')
    const { test } = useParams()
    const [tes, setTes] = useState(test)
    const [error, setError] = useState('')
    const [mail, setMails] = useState([{}])
    const   [send, setSend] = useState('')
    console.log(userTest[0])
    console.log(tes)

    async function getTest() {
        const readTest = await axios.get(`${SERVER_URL}/usertest?id=${test}`,{ headers: { authorization: localStorage.token||sessionStorage.token } })
        console.log(readTest.data)
        setUserTest(readTest.data)

    }
    

    


    // async function getTestDetails() {
    //     const readTestDetails = await axios.get(`http://localhost:4000/testsdetails?id=${test}`)
    //     console.log(readTestDetails.data)
    //     setTestDetails(readTestDetails.data)
    // }

    async function mails(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        console.log(values)
        await updateMails(values)

    }

    async function updateMails(values) {
        try {
            const num = Object.keys(values).length
            // console.log(num)
            const arr = [...Array(num)]
            // console.log(arr)
            const newArr = arr.map((i, index) => {
                return values[index]
            })
            console.log(newArr)

            // console.log(tes)
            const mailing_list = await axios.post(`${SERVER_URL}/updatetest?id=${tes}`, newArr,  { headers: { authorization: localStorage.token||sessionStorage.token } })

            // console.log(mailing_list.data)
            const publish = await axios.get(`${SERVER_URL}/publishtest?id=${tes}`,{ headers: { authorization: localStorage.token||sessionStorage.token } })
            // console.log(publish.data)

            await setSend('The test was sent successfully!')

        }
        catch (e) {
            // console.log(e.response.data.error || e.message)
            // debugger
            
            setError(e.response.data.error.name ? `you can't send the test twice to the same person` : e.response.data.error)
        }

    }


    async function addMail(e) {
        e.preventDefault()
        setMails([...mail, {}])
    }

    useEffect(getTest, [])
    // useEffect(getTestDetails, [])

    return userTest ?
            <div className="doneTest">
                <div>
                    <h1 className="titles"> {userTest[0].name} </h1>
                    <h3 className="titles"> {userTest[0].discription}</h3>
                </div>
                <form>
                    {userTest[1].map(item =>
                        <div className="question">
                            <h3 className="titles">{item.name}</h3>
                            <h4 className="titles">{item.Instructions}</h4>
                            <div className="answers">
                                {item.answers.map(ans =>
                                    <label  className="answer">
                                        {ans.discription} {`    `}
                                        <input
                                            type={item.ques_type}
                                            name={ans}
                                            // defaultChecked={testDetails.sketch.find(item => item.answer == ans._id)}
                                           
                                        />
                                    </label>

                                )}
                            </div>
                        </div>
                    )}
                    <hr></hr>
                </form>

                <form className="mailing" onSubmit={mails}>
                {mail.map((item, index) =>

                <div>
                        <div className="mails">
                        <input className="testInput" type="text" placeholder='add student email' name={index} />
                        <button className="newTest" onClick={addMail}>add email</button>
                        </div>
                    </div>
                )}
                {!send ? <input className="newTest" type="submit" value='send test' /> : ''}
                <div>{send}</div>
                <div>{error}</div>
                {/* {questionsList.length > 0  ? <input type="submit" value='send test' /> : ''} */}

            </form>
            </div> : 'loading'
}