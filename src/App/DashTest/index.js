import { useParams, useEffect, useState } from "react";
import { Route, Switch, Link } from "react-router-dom"
import axios from "axios";
import { useContext } from 'react';
import { LoginContext } from '../../Login'
import { SERVER_URL } from "../../globals";




export default function DashTest() {
    const [dash, setDash] = useState()
    const user = useContext(LoginContext)
    const [status, setStatus] = useState('ready')
    // console.log(user)
    // const { id } = useParams()

    async function fetchData() {

        const res = await axios.get(`${SERVER_URL}/${status == 'ready' ? 'mytest' : 'testdone'}?id=${user[0]._id}`)
        console.log(res.data)
        setDash(res.data)
    }




    function ready() {
        setStatus('ready')
    }
    function done() {
        setStatus('done')
    }

    useEffect(fetchData, [status])

    return dash ?
        <div>
            <div className="testIneed">
                <h1 className="title">{status == 'ready' ? 'test i need to do' : 'test i did'} </h1>
                <div className="myTestButton">
                <button className="newTest" onClick={ready}>X</button>
                <button className="newTest" onClick={done} >V</button>
                </div>
            </div>
            <hr></hr>
            <table>
                <tr>
                    <th>test name</th>
                    <th>tester name</th>
                    <th>{status == 'ready' ? 'submission date' : 'grade'}</th>
                    

                </tr>
                {dash.map(item => (
                    <tr>
                        {status == 'ready' ?
                            <Link to={`/test/${item.test_id}`}>
                                <td>{item.test_name}</td>
                                
                            </Link>
                                
                            :
                            <Link to={`/test/${item.test_id}`}>
                                <td>{item.test_name}</td>
                                
                            </Link>
                            }
                            <td>{item.tester_name}</td>
                             {status == 'ready' ? <td>  {item.submission_date} </td> : 
                            <td>{item.grade}</td>}
                    </tr>
                ))}
            </table>
        </div>

        : 'loading'
}