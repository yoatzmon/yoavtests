import axios from "axios"
import { createContext, useContext, useEffect, useState } from "react"
import { DeletContext } from ".."
// import Answer from './Answer'
import { SERVER_URL } from "../../../globals"
export default function Questions({ param }) {



    const [answersList, setAnswersList] = useState([])
    const [questionData, setQuestionData] = useState('')
    const [del, setDel] = useContext(DeletContext),
        [error, setError] = useState(''),
        [ques, setSaveQues] = useState('')

    console.log(answersList);

    async function getAnswers() {

        const res = await axios.get(`${SERVER_URL}/getanswer?id=${param}`)
        console.log(res.data.answers)
        setQuestionData(res.data)
        setAnswersList(res.data.answers)
    }

    function addAnswer(e) {
        e.preventDefault()

        setAnswersList([...answersList, { discription: "", score: '' }])
    }

    function removeAnswer(e, index) {
        e.preventDefault()
        const rlrlr = [...answersList]
        if (rlrlr.length != 1)
            rlrlr.splice(index)
        setAnswersList(rlrlr)
    }
    //     console.log(res.data.ques_type)

    async function saveQuestion(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )
        // createQuestion(values)
        // delete values.Instructions,delete values.name,delete values.ques_type


        console.log(values)

        await updateQuestion(values)
    }

    async function updateQuestion(values) {
        try {
            const num = Object.keys(values).length - 4
            // console.log(num)
            const arr = [...Array(num)]
            // console.log(arr)
            const newArr = arr.map((i, index) => {
                return {
                    discription: values[index],
                    score: values[index + 10]
                }
            })
            console.log(newArr);
            newArr.splice(newArr.length / 2, newArr.length / 2)

            const list = {

                Instructions: values.Instructions,

                name: values.name,

                ques_type: values.ques_type,

                score: values.score,

                answers:

                    // {"discription" : `${values[0]}`},
                    // {"discription" : `${values[1]}`},
                    // {"discription" : `${values[2]}`},
                    // {"discription" : `${values[3]}`}
                    [...newArr]

            }
            console.log(list);

            const res = await axios.put(`${SERVER_URL}/updatequestion?id=${param}`, list)
            console.log(res.data)
            setError('')
            setSaveQues('question saved!')
        }
        catch (e) {
            setSaveQues('')
            console.log(e.response.data.error)
            // debugger
            setError(e.response.data.error)
        }

    }

    async function deletQuestion(e) {
        e.preventDefault()
        console.log(param)
        await axios.delete(`${SERVER_URL}/deletquestion?id=${param}`)
        setDel([...del, {}])

    }

    // useEffect(deletQuestion,[])
    useEffect(getAnswers, [])

    return <div className='new_question'>
        <form onSubmit={saveQuestion}>
            <input className="questionInput" id="name" type="text" placeholder="enter the question" required name="name" defaultValue={questionData ? questionData.name : ''} /><br></br>
            <input className="questionInput" id="Instructions" type="text" placeholder="enter the Instructions" required name="Instructions" defaultValue={questionData ? questionData.Instructions : ''} /><br></br>
            <input className="questionInput" id="score" type="text" placeholder="enter the score" required name="score" defaultValue={questionData ? questionData.score : ''} /><br></br>
            <select className="questionInput" name="ques_type" id="ques_type"  defaultValue={questionData ? questionData.ques_type : ''}>
                <option value="radio">radio</option>
                <option value="checkbox">checkbox</option>
            </select><br></br>
            {
                answersList.map((item, index) =>
                    <div>
                        <input className="answerInput" type='text' placeholder='enter answer' defaultValue={item.discription} name={index} />
                        <input className="answerInput" type='score' placeholder='enter score' defaultValue={item.score} name={'1' + String(index)} />
                        {/* <label for="true">true</label><input type="checkbox"  name={'1' + String(index)} id="true"/> */}
                        <button  className="newTest" onClick={(e) => addAnswer(e)}>+</button>
                        <button  className="newTest" onClick={(e) => removeAnswer(e, index)}>-</button></div>
                )}
            <div className="addRemove">
            <button  className="addRemoveButtun" onClick={deletQuestion}>delete question</button>
            <input  className="addRemoveButtun" type="submit" value="save question" /><br></br>
            </div>
            <div>{ques}</div>
            <div>{error}</div>
        </form>

    </div>
}