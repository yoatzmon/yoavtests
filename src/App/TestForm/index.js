import axios from "axios"
import Question from './Questions'
import { createContext, useEffect, useState } from "react"
import { useParams, useHistory } from "react-router-dom"
import { SERVER_URL } from "../../globals"
export const DeletContext = createContext()

export default function TestForm() {

    const History = useHistory()
    const { testId } = useParams()
    console.log(testId)

    const [tes, setTes] = useState(testId),
        [val, setVal] = useState(),
        [mail, setMails] = useState([{}]),
        [questionsList, setQuestionsList] = useState(''),
        delQues = useState(''),
        [del, detDel] = delQues,
        [error, setError] = useState(''),
        [err, setErr] = useState(''),
        [send, setSend] = useState('')
    console.log(questionsList)
    // console.log(testId)

    async function save(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        console.log(values)
        await newTest(values)

    }
    async function newTest(values) {
        try{
            setErr('')
        if (tes) {

            const ser = await axios.post(`${SERVER_URL}/updatetest?id=${tes}`, values, { headers: { authorization: localStorage.token||sessionStorage.token } })

            // console.log(ser.data)
        }
        else {
            const res = await axios.post(`${SERVER_URL}/createtest`, values, { headers: { authorization: localStorage.token||sessionStorage.token } })
            console.log(res.data)
            setTes(res.data._id)
        }
        // let test;
        // const res = await axios.post(`${SERVER_URL}/${testId ? `updatetest?id=${testId}`: `createtest`}`, values)
        // test = res.data
       // console.log(res.data)
    }
    catch(e){
        // debugger
        setErr(e.response.data)
    }
    }
    useEffect(async () => {
        const value = await axios.get(`${SERVER_URL}/test?id=${testId}`)
        console.log(value.data)
        setVal(value.data)
    }, [])


    async function createQuestions() {
        console.log(localStorage.token)
        const new_question = await axios.get(`${SERVER_URL}/createquestion?id=${tes}`,{ headers: { authorization: localStorage.token||sessionStorage.token }})
        console.log(new_question.data)
        await displayQuestions()
    }

    async function displayQuestions() {
        const question = await axios.get(`${SERVER_URL}/question?id=${tes}`,{ headers: { authorization: localStorage.token||sessionStorage.token } })
        setQuestionsList(question.data)
    }
    // async function deletQuestions(){

    // }


    async function mails(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        console.log(values)
        await updateMails(values)

    }

    async function updateMails(values) {
        try {
            const num = Object.keys(values).length
            // console.log(num)
            const arr = [...Array(num)]
            // console.log(arr)
            const newArr = arr.map((i, index) => {
                return values[index]
            })
            // console.log(newArr)

            // console.log(tes)
            const mailing_list = await axios.post(`${SERVER_URL}/updatetest?id=${tes}`, newArr,  { headers: { authorization: localStorage.token||sessionStorage.token } })

            // console.log(mailing_list.data)
            const publish = await axios.get(`${SERVER_URL}/publishtest?id=${tes}`,{ headers: { authorization: localStorage.token||sessionStorage.token } })


            await setSend('The test was sent successfully!')

        }
        catch (e) {
            // console.log(e.response.data.error || e.message)
            // debugger
            setError(e.response.data.error)
        }

    }




    async function addMail(e) {
        e.preventDefault()
        setMails([...mail, {}])
    }



    useEffect(displayQuestions, [del])

    return <div>
        <form className="makeTest" onSubmit={save}>

            <input className="testInput" type="text" name="name" placeholder="enter the test name" required defaultValue={val ? val.name : ''} /><br></br>
            <input className="testInput" type="text" name="discription" placeholder="enter the test discription" required defaultValue={val ? val.discription : ''} /><br></br>
            <label>DeAd LiNe</label><input className="testInput" type="date" name="submission_date" required defaultValue={val ?  val.submission_date : ''} /> 
            <input  className="newTest" type="submit" value="save test" />
            <div>{err}</div>
        </form>
        <hr></hr>
        <div className="aligen">
        {tes && !err ? <button class="addQues" onClick={createQuestions}>add ques</button> : ''}
        <div>{error}</div>
        {/* <button onClick={deletQuestions}>remove ques</button> */}
        <div className="all_ques">
            {tes ? (questionsList ?
                questionsList.map(item =>
                    // <div>
                        <DeletContext.Provider value={delQues}>
                            <Question param={item._id} />
                        </DeletContext.Provider>
                    // </div>
                )
                : '')
                : ''}
                </div>
                    {questionsList.length > 0 ?
            <form className="mailing" onSubmit={mails}>
                {mail.map((item, index) =>

                <div>
                        <div className="mails">
                        <input className="testInput" type="text" placeholder='add student email' name={index} />
                        <button className="newTest" onClick={addMail}>add email</button>
                        </div>
                    </div>
                )}
                {!send ? <input className="newTest" type="submit" value='send test' /> : ''}
                <div>{send}</div>
                {/* {questionsList.length > 0  ? <input type="submit" value='send test' /> : ''} */}

            </form> : ''}

                </div>
    </div>

}







