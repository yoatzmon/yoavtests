import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import Login from './Login'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <Login>
     <App/>
     </Login>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
)